# Bálint Séra


<img width="150" style="float: left;margin-right: 30px" src="img/sb-2023.png">


 * Szeged
 * __T:__ +36-70-637-9022
 * __@:__ balint.sera@gmail.com 

<br style="clear:both">

## Software Engineer, Cloud Architect

### Extensive experience in R&D, Product Development, Engineering, DevOps, Mentoring

Experienced full stack developer and solutions architect with a demonstrated 15+ years history of working in security, marketing and advertising industry. Strong engineering professional skilled in AWS, Dotnet/C#, Javascript, PHP.


## Areas of expertise

* Solution Architecting
* Developing in multiple languages and frameworks (.Net/Dotnet Core, Javascript (Node.js), PHP)
* Leading engineering teams
* Mentoring


## Certifications

* Certified in Cybersecurity (ISC2) (2024)
* AWS Certified Solutions Architect - Professional (2022)
* AWS Certified Developer - Associate
* AWS Certified Solutions Architect - Associate

## Professional experience

### Staff Software Engineer, Solution Architect, 2023-, LastPass

Helped enhancing LastPass' security posture in various areas in the product with security fixes and features, obtained a security certification. Migrated different components from GoTo's platforms to LastPass: microservice pipelines from TeamCity to GitLab, observability from Splunk to DataDog etc. 



### Senior Software Engineer, Solution Architect, 2020-2023, LastPass (GoTo, formally LogMeIn)

Led the design and the development of multiple AWS hosted serverless solutions and microservices that provided the backend of the Security Dashboard of LastPass clients and other smaller projects and features including security related ones. Built pipelines and local environments for multiple projects/teams.

Learnt how:

* a security product is managed, developed and operated
* to design, build and operate secure, highly available, elastic, scalable services that can serve thousands of requests per seconds - in practice
* to support multiple types of clients (mobile, web, cli etc.) while adding new features
* be heard and appreciated in a large enterprise

### Senior Software Engineer, Solutions Architect, 2018-2020, Red Steed Studio

I participated in the development of a highly available API for a Taiwan and Hong-Kong based media company (node.js, MongoDB, docker, AWS) and then helped coding the API (node.js, TypeScript), the Unity client (C#) and building/managing the AWS infra of a music related mobile application for a Silicon Valley based startup. 

### Head of Technology 2015-2018, Evista Creative Agency

Managed the transformation of this middle sized (<50) creative agency from a rather naive developer shop to a professional software developer studio via agile tools and methodologies. This transformation made the company more attractive to enterprise customers and at the same time to professional developers, too. These two factor multiplied the profitability of the company.

Developed various applications using PHP and Javascript, built pipelines and supported the operations teams. Jofogas.hu was the largest brand.
### Senior web developer 2014-2015, Outlast Kft.

Mostly full stack web development tasks for famous brands like UPC and L'Oreal using strict enterprise methodologies and rules from code formatting to continuous delivery (in PHP and Javascript).

Learnt a lot at this Texas expat founded small but professional company about how to use modern software development and leadership practices - and more importantly, about how to abuse both.

### Lead web developer 2014-2011, Skyshield

Full stack developer of a GPS vehicle tracking system using a dedicated mobile device, PHP, javascript and some dirty server-side hack. Including Drupal and Symfony.

First time leading a team of 4 junior programmers.

### 5+ years of freelancing, enterpreneurship

Full stack developer, freelancer and enterpreneur. Interesting gigs like the brand new faceted search engine of Laptop.hu that was advertized on billboards and even in one of a country wise radio, or many of the skins for Index.hu's most popular blogs.

## Hobbies

1. Cooking
2. Gardening
3. Interior design and arts
4. And I have a daughter, so everything she prefers to do/play

Do you have this as a pdf? Please visit the original updated version here: https://gitlab.com/damnadm/cv